export const config = {
  token: process.env.APP_TOKEN || 'DEFAULT_TOKEN',
  delay: process.env.DELAY && +process.env.DELAY,
  bibleBaseUrl: process.env.BIBLE_BASE_URL
}
