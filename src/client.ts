import axios from 'axios'
import { config } from './config'

interface BibleClientGetResponse {
  result: {
    text: string
    reference: string
  }
}

export class BibleClient {
  httpClient = axios.create({
    baseURL: config.bibleBaseUrl
  })

  async get() {
    const response = await this.httpClient.get<BibleClientGetResponse>('')

    return response
  }
}
