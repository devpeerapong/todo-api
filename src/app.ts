import express from 'express'
import morgan from 'morgan'

import * as controller from './controller'
import { validateToken } from './middleware'

const app = express()

app.use(express.json())
app.use(morgan('combined'))

app.get('/kill', controller.kill)

app.use(validateToken)

app.get('/', controller.list)
app.post('/', controller.create)
app.post('/:id/done', controller.done)
app.delete('/:id', controller.del)

export { app }
