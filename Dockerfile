FROM node:12-alpine AS builder
WORKDIR /api
COPY package.json yarn.lock ./
RUN yarn
COPY tsconfig.json ./
COPY src ./src
RUN yarn build

FROM node:12-alpine
WORKDIR /api
COPY --from=builder /api/package.json /api/yarn.lock ./
RUN yarn --prod
COPY --from=builder /api/build ./build
EXPOSE 3000
CMD ["node", "build/server.js"]