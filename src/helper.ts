import { config } from './config'

async function delay(second: number) {
  return new Promise(res => setTimeout(res, second * 1000))
}

function random() {
  return Math.floor(Math.random() * Math.floor(21))
}

export async function setup() {
  const seconds = config.delay || random()
  console.log(`waiting for ${seconds} second(s) before start server`)
  await delay(seconds)
}
