import { Request, Response } from 'express'
import { BibleClient } from './client'

type Todo = { text: string; done: boolean; id: string }

const todos: Record<string, Todo> = {}
const client = new BibleClient()

export const kill = async (_: Request, res: Response) => {
  console.log('killing process')
  process.exit(1)
}

export const list = async (_: Request, res: Response) => {
  try {
    const verse = await randomBibleVerse()
    const result = {
      verse,
      todo: Object.values(todos)
    }

    res.status(200).json({ result })
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}

export const create = (req: Request, res: Response) => {
  const id = uuid()
  const todo = { ...req.body, done: false, id }

  todos[id] = todo

  res.status(200).json({ result: todo })
}

export const done = (req: Request, res: Response) => {
  const { id } = req.params
  const todo = { ...todos[id], done: true }

  todos[id] = todo

  res.status(200).json({ result: todo })
}

export const del = (req: Request, res: Response) => {
  const id = req.params.id

  delete todos[id]

  res.status(200).json({ result: Object.values(todos) })
}

function uuid() {
  return Math.random()
    .toString(32)
    .substr(2)
}

async function randomBibleVerse() {
  const { data } = await client.get()

  return data.result
}
