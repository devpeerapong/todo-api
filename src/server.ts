import http from 'http'
import { config } from 'dotenv'
config()

import { app } from './app'
import { createTerminus } from '@godaddy/terminus'
import { setup } from './helper'

const port = 3000

const server = http.createServer(app)

async function onSignal() {
  console.log('SIGTERM received, cleanup server.')
  console.log('Shutting Down...')
}

async function onShutdown() {
  console.log('Shut Down')
}

function health() {
  return Promise.resolve()
}

const healthChecks = {
  '/health': health
}

createTerminus(server, { onSignal, onShutdown, healthChecks })
;(async () => {
  await setup()

  server.listen(port).on('listening', () => {
    console.log('app running at port 3000')
  })
})()
