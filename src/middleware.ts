import { Request, Response, NextFunction } from 'express'
import { config } from './config'

export function validateToken(req: Request, res: Response, next: NextFunction) {
  if (
    !req.headers.authorization ||
    req.headers.authorization !== config.token
  ) {
    res.status(401).json({ result: 'Unauthorize' })
    return
  }

  next()
}
